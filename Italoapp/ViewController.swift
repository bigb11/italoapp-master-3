//
//  ViewController.swift
//  Italoapp
//
//  Created by domenico.maione on 05/12/2019.
//  Copyright © 2019 domenico.maione. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
   
    
    @IBOutlet weak var buttonDate1: UIButton!
    
    @IBOutlet weak var buttonDate2: UIButton!
    
    @IBOutlet weak var switchButton: UISwitch!
    
    
    @IBOutlet weak var textField1: UITextField!
    
    @IBOutlet weak var textField2: UITextField!
    
    @IBOutlet weak var swapButton: UIButton!
    
        
    let pickerRound: UIPickerView = UIPickerView()
    let picker: UIDatePicker = UIDatePicker()
    var flag = 0
    var flag2 = 0
    
    
    
    
    
    
   
    let arrayRound: Array<String> = ["Bergamo", "Bologna","Bolzano", "Brescia","Conegliano", "Desenzano","Ferrara", "Firenze SMN", "Milano (All)", "Milano Centrale", "Milano Rho Fiera", "Milano Rog", "Napoli Afragola", "Napoli C.le", "Padova", "Peschiera", "Pordenone", "Reggio Emilia AV", "Roma (All)", "Roma Termini", "Roma Tib", "Rovereto", "Rovigo", "Salerno", "Torino P.Nuova", "Torino P.Susa", "Trento", "Treviso C.le", "Udine", "Venezia (All)", "Venezia Mestre", "Venezia S.Lucia", "Verona P.N", "Vicenza"]
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        buttonDate1.tag = 1
        buttonDate2.tag = 2
        
        if(picker.isHidden == false){
            textField1.inputView = pickerRound
            textField2.inputView = pickerRound
        }
        
        
        if(pickerRound.isHidden == false){
        pickerRound.dataSource = self
        pickerRound.delegate = self
        }
        buttonDate1.setTitle("/-/-/", for: .normal)
        buttonDate2.setTitle("/-/-/", for: .normal)
        self.navigationController?.navigationBar.topItem?.title = "Home"
        
        pickerRound.isUserInteractionEnabled = false
        //navigationController?.navigationBar.topItem?.title = tabBarItem.title;
        
        //self.tabBarController?.navigationItem.title = tabBarItem.title
        
        
       let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissPicker")
        
        view.addGestureRecognizer(tap)
        
        
       
        
        
    }
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    //Restituisce il numero totale degli elementi dell'array
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return arrayRound.count
    }
        
    
    //Carica i valori dell'array nel picker
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        picker.isHidden = true
        pickerRound.isUserInteractionEnabled = true
        
        return arrayRound[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         
        pickerRound.isUserInteractionEnabled = true
        
        
        if(textField1.isFirstResponder){
            
        
            textField1.text = arrayRound[row]
            
            pickerRound.isUserInteractionEnabled = false
            
            
        
            
        }
        
        else{
                
                textField2.text = arrayRound[row]
            
            pickerRound.isUserInteractionEnabled = false
                
           
                
        
                
        }
        
        
        
    }
    
    


    
    @IBAction func clickOnButton(_ sender: UIButton) {
        let tempPos: CGPoint = textField1.frame.origin
        UIView.beginAnimations(nil, context: nil)
        textField1.frame.origin = textField2.frame.origin
        textField2.frame.origin = tempPos
        UIView.commitAnimations()
        
    }
    
    
    
    @IBAction func clickOnButtondate(_ sender: UIButton) {
        
        
        if(pickerRound.isUserInteractionEnabled == false){
            
            if(sender.tag == 1 || (sender.tag == 2 && flag2 == 1)){
        picker.isHidden = false
        picker.tag = sender.tag
        picker.datePickerMode = .dateAndTime
        picker.addTarget(self, action: #selector(dueDateChanged(sender:)), for: UIControl.Event.valueChanged)
            
        let pickerSize : CGSize = picker.sizeThatFits(CGSize.zero)
            picker.frame = CGRect(x:50, y:550, width:pickerSize.width, height:350)
            // you probably don't want to set background color as black
            // picker.backgroundColor = UIColor.blackColor()
        
        
            self.view.addSubview(picker)
        
            }
            
        }
    
    
    
    }
    
    @objc func dueDateChanged(sender: UIDatePicker){
        
        let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
        
        switch sender.tag {
        case 1: buttonDate1.setTitle(dateFormatter.string(from: sender.date), for: .normal)
        
       
            picker.isHidden = true
            break
            
        case 2: buttonDate2.setTitle(dateFormatter.string(from: sender.date), for: .normal)
        //picker.isHidden = true
            break
            
        default:
            break
        }
            
    
        
        
    }
    
    
    

    @IBAction func switchIsTapped(_ sender: UISwitch) {
        
        if(sender.isOn){
            
            flag2 = 1
            buttonDate2.setTitleColor(.darkText, for: .normal)
            buttonDate2.tintColor = .black
            
        }
        else{
               flag2 = 0
            
            buttonDate2.setTitleColor(.opaqueSeparator, for: .normal)
                buttonDate2.tintColor = .opaqueSeparator
            
            
            
            buttonDate2.setTitle("/-/-/", for: .normal)
            picker.isHidden = true
                
        }
    }
    
    
    
    
    @objc func dismissPicker() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    


}

